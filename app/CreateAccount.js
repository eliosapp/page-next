import request from "@/app/request"

const CreateAccount = async (json) => {
    const respond = await request({
        method : "POST", 
        json, 
        rute : "user"
    })
    return respond
}
export default CreateAccount